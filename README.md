# Docker Image for DBT

A Docker image for [dbt (data build tool)](https://github.com/fishtown-analytics/dbt) configured for use with Dremio.

## Requirements

## Getting Started

You can run the Dockerized `dbt` command by executing the following command:

```bash
docker run --rm -it \
    -v $PWD:/usr/app \
    -v /path/to/your/profiles.yml:/root/.dbt/profiles.yml \
    ryanlatini/dbt-dremio:latest run
```

## License

MIT License Copyright (c) 2021 Ryan Latini
